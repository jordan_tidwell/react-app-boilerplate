import React, { Component } from 'react';
import Card from './Components/Card';
import HttpReq from './Services/HttpReq';
import logo from './logo.svg';
import './App.css';




class App extends Component {

  componentDidMount()
  {
    let req = HttpReq.get( 'https://api.github.com/repos/git/git/contributors' );

    req.then(( resp ) => this.setState({ githubUsers: resp.data }) )
    .catch( (error) => console.log( error ) );

  }

  render() {
    return (
      <div className="App">
          <img src={logo} style={ { width: '100px', margin: '0 auto', display: 'block' } } className="App-logo" alt="logo" />
          <Card headline={ 'Hello, World!' } description={ 'This is just a card element.' } />
      </div>
    );
  }
}

export default App;
