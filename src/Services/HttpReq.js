import axios from 'axios';

const HttpReq = {
    post: ( url, data ) => {
        return axios.post(url, data);
    },
    get: ( url ) => {
        return axios.get( url );
    }
}

export default HttpReq;