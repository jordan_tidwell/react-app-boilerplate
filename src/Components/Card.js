import React, { Component } from 'react';

const Card = ( props ) => {
    return(
        <div className={ 'card' }>
            <h3 className={ 'card__headline' }>{ props.headline }</h3>
            <p className={ 'card__description' }>{ props.description }</p>
        </div>
    );
}

export default Card;